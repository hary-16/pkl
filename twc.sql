-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 18 Agu 2020 pada 05.07
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `twc`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `id` int(10) UNSIGNED NOT NULL,
  `kategori` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id`, `kategori`, `created_at`, `updated_at`) VALUES
(1, 'Resiko Finansial', NULL, NULL),
(2, 'Resiko Operasional', NULL, NULL),
(3, 'Resiko Reputasi', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `level`
--

CREATE TABLE `level` (
  `id` int(10) UNSIGNED NOT NULL,
  `level` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4, '2020_07_20_030036_create_owner_table', 2),
(5, '2020_07_20_030115_create_dept_table', 2),
(49, '2014_10_12_000000_create_users_table', 3),
(50, '2014_10_12_100000_create_password_resets_table', 3),
(51, '2019_08_19_000000_create_failed_jobs_table', 3),
(52, '2020_07_20_030228_create_level_table', 3),
(53, '2020_07_27_033117_create_proses_table', 3),
(54, '2020_07_27_033534_create_registers_table', 3),
(55, '2020_07_27_033557_create_kategori_table', 3),
(56, '2020_07_27_044349_create_unit_table', 3),
(57, '2020_07_27_044540_create_owners_table', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `owners`
--

CREATE TABLE `owners` (
  `id` int(10) UNSIGNED NOT NULL,
  `owners` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `owners`
--

INSERT INTO `owners` (`id`, `owners`, `created_at`, `updated_at`) VALUES
(1, 'SM TI', NULL, NULL),
(2, 'Manajer Aplikasi Bisnis', NULL, NULL),
(3, 'Manajer Jaringan Komputer dan Perangat Keras', NULL, NULL),
(4, 'Manajer Perangkat Keras', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `proses`
--

CREATE TABLE `proses` (
  `id` int(10) UNSIGNED NOT NULL,
  `proses` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `proses`
--

INSERT INTO `proses` (`id`, `proses`, `created_at`, `updated_at`) VALUES
(1, 'Perencanaan Program Kerja IT', NULL, NULL),
(2, 'Manajemen Keamanan Data & Informasi', NULL, NULL),
(3, 'Desain Sistem Informasi', NULL, NULL),
(4, 'Desain Sistem dan Jaringan', NULL, NULL),
(5, 'Pemeliharaan & Perawatan Perangkat Keras', NULL, NULL),
(6, 'Sistem Monitoring Infrastruktir TI', NULL, NULL),
(7, 'Tata Kelola TI', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `registers`
--

CREATE TABLE `registers` (
  `id` int(10) UNSIGNED NOT NULL,
  `tujuan` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `proses` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `event` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `penyebeb` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sumber` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `potensi` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owners` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `likelihoodir` int(11) NOT NULL,
  `impactir` int(11) NOT NULL,
  `levelir` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ada` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `memadai` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dijalankan` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `likelihoodrr` int(11) NOT NULL,
  `impactrr` int(11) NOT NULL,
  `levelrr` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `perlakukan` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tindakan` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `likelihoodram` int(11) NOT NULL,
  `impactram` int(11) NOT NULL,
  `levelram` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `registers`
--

INSERT INTO `registers` (`id`, `tujuan`, `proses`, `kategori`, `kode`, `event`, `penyebeb`, `sumber`, `potensi`, `owners`, `unit`, `likelihoodir`, `impactir`, `levelir`, `ada`, `memadai`, `dijalankan`, `likelihoodrr`, `impactrr`, `levelrr`, `perlakukan`, `tindakan`, `likelihoodram`, `impactram`, `levelram`, `created_at`, `updated_at`) VALUES
(1, 'Efektivitas dan Efisiensi dalam Perencanaan dan Pengelolaan Project IT', 'Perencanaan Program Kerja IT', 'Risiko Finansial', 'R1', 'Project Management yang tidak Standar', 'Pengetahuan mengenai Project Management yang terbatas\r\n', 'Internal', 'Inefisiensi dan Inefektivitas Implementasi Project IT yg Dilakukan\r\n', 'SM TI', 'IT', 2, 2, 'Medium', 'Weekly meeting Direktorat Infrastruktur & TI\r\n', 'Belum memadai', 'Dijalankan 100%', 2, 2, 'Medium', 'Reduce/ Memitigasi risiko', 'Pemberlakuan sistem project management pada seluruh project IT\r\n', 1, 1, 'Low', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `unit`
--

CREATE TABLE `unit` (
  `id` int(10) UNSIGNED NOT NULL,
  `unit` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `unit`
--

INSERT INTO `unit` (`id`, `unit`, `created_at`, `updated_at`) VALUES
(1, 'IT', NULL, NULL),
(2, 'Semua Statker', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Haryanto', 'haryanto1606@gmail.com', NULL, '$2y$10$4xCY6oktxdrna1t2ikRdHOUqW/owx/DzZ6Oi/hNvcyDsEE47FVb.y', NULL, '2020-08-09 18:51:05', '2020-08-09 18:51:05'),
(2, 'Haryanto', 'haryanto@gmail.com', NULL, '$2y$10$6ITqz6LXl8PuMFZuW9W6NezOmdDCoE8WEozAsXobDuHk93GFm2.pq', NULL, '2020-08-09 18:58:24', '2020-08-09 18:58:24'),
(3, 'aaa', 'aaa@mail.com', NULL, '$2y$10$iPRvYm1LCUTxh2DU.Pcm0OqMvJ1CJGwvqCgELL4HEy4XU1KplKoP2', NULL, '2020-08-09 19:00:47', '2020-08-09 19:00:47'),
(4, 'aaaa', 'haryanto606@gmail.com', NULL, '$2y$10$C6lJncaCGCEcKA.7lBHBD.KNnKMzuXg/ZEh6cca.9.M7EvrWJg.8S', NULL, '2020-08-09 19:02:44', '2020-08-09 19:02:44');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `owners`
--
ALTER TABLE `owners`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `proses`
--
ALTER TABLE `proses`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `registers`
--
ALTER TABLE `registers`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `level`
--
ALTER TABLE `level`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT untuk tabel `owners`
--
ALTER TABLE `owners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `proses`
--
ALTER TABLE `proses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `registers`
--
ALTER TABLE `registers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `unit`
--
ALTER TABLE `unit`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
