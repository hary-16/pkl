<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registers', function (Blueprint $table) {
            $table->Increments('id');
			$table->string('tujuan',150);
			$table->string('proses',50);
			$table->string('kategori',50);
			$table->string('kode',5);
			$table->string('event',50);
			$table->string('penyebeb',150);
			$table->string('sumber',10);
			$table->string('potensi',150);
			$table->string('owners',100);
			$table->string('unit',20);
			$table->integer('likelihoodir');
			$table->integer('impactir');
			$table->string('levelir',10);
			$table->string('ada',150);
			$table->string('memadai',20);
			$table->string('dijalankan',20);
			$table->integer('likelihoodrr');
			$table->integer('impactrr');
			$table->string('levelrr',10);
			$table->string('perlakukan',100);
			$table->string('tindakan',100);
			$table->integer('likelihoodram');
			$table->integer('impactram');
			$table->string('levelram',10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registers');
    }
}
