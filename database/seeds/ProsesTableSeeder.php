<?php

use Illuminate\Database\Seeder;

class ProsesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     DB::table('proses')->insert([
		['proses'=>'Perencanaan Program Kerja IT'],
		['proses'=>'Manajemen Keamanan Data & Informasi'],
		['proses'=>'Desain Sistem Informasi'],
		['proses'=>'Desain Sistem dan Jaringan'],
		['proses'=>'Pemeliharaan & Perawatan Perangkat Keras'],
		['proses'=>'Sistem Monitoring Infrastruktir TI'],
		['proses'=>'Tata Kelola TI']
	]);
    }
}
