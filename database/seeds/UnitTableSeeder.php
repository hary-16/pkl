<?php

use Illuminate\Database\Seeder;

class UnitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     DB::table('unit')->insert([
		['unit'=>'IT'],
		['unit'=>'Semua Statker']
	]);
    }
}
