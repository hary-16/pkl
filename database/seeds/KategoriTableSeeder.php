<?php

use Illuminate\Database\Seeder;

class KategoriTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     DB::table('kategori')->insert([
		['kategori'=>'Resiko Finansial'],
		['kategori'=>'Resiko Operasional'],
		['kategori'=>'Resiko Reputasi'],
	]);
    }
}
