<?php

use Illuminate\Database\Seeder;

class OwnersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     DB::table('owners')->insert([
		['owners'=>'SM TI'],
		['owners'=>'Manajer Aplikasi Bisnis'],
		['owners'=>'Manajer Jaringan Komputer dan Perangat Keras'],
		['owners'=>'Manajer Perangkat Keras']
	]);
    }
}
