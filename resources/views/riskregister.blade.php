@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h4>Daftar Risk Register</h4>
@stop

@section('content')
<a href="/" class="btn btn-info btn-sm">Tambah Risk Regster</a>
    <table class="table table-responsive martop-sm">
	<thead>
		 <th>No</th>
		 <th>Objective/Tujuan</th>
		 <th>Proses Bisnis</th>
		 <th>Risk Category</th>
		 <th>Kode Risiko</th>
		 <th>Risk Event</th>
		 <th>Risk Couse</th>
		 <th>Sumber Risiko</th>
		 <th>Severity/Akibat</th>
		 <th>Potensi Resiko</th>
		 <th>Pemilik Resiko</th>
		 <th>Unit Terkait</th>
		 <th>Score/Nilai Inherent Risk</th>
		 <th>Likelihood</th>
		 <th>Impact</th>
		 <th>Tingkat Risiko</th>
		 <th>Existring Control</th>
		 <th>Ada/Tidak Ada</th>
		 <th>Memadai/Belum Memadai</th>
		 <th>Dijalankan 100%/Belum dijalankan 100%</th>
		 <th>Score/Nilai Residual Risk</th>
		 <th>Likelihood</th>
		 <th>Impact</th>
		 <th>Tingkat Resiko</th>
		 <th>Opsi Perlakuan Risiko</th>
		 <th>Deskripsi Tindakan Mitigasi</th>
		 <th>Nilai Target</th>
		 <th>Likelihood</th>
		 <th>Impact</th>
		 <th>Tingkat Risiko</th>
    </thead>
    <tbody>
		@foreach ($reg as $r)
			<tr>
			<td>{{ $r->tujuan }}</td>
			<td>{{ $r->kategori }}</td>
			<td>{{ $r->kode }}</td>
			<td>{{ $r->event }}</td>
			<td>{{ $r->penyebab }}</td>
			<td>{{ $r->sumber }}</td>
			<td>{{ $r->potensi }}</td>
	
            </tr>
		@endforeach
	</tbody>
	</table>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop