@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Risk Register</h1>
@stop

@section('content')
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

<div class="form-group">
	<label for="tujuan" class="control-label">Objective/Tujuan</label>
	<input type="text" class="form-control" name="tujuan"
	placeholder="Objective/Tujuan">
 </div>
 <div class="form-group">
	<label for="proses">Proses Bisnis</label>

	<select class="form-control" name="kode">
		<option value="">--pilihan--</option>
		<option value="1">Perencanaan Program Kerja IT</option>
		<option value="2">Manajemen Keamanan Data & Informasi</option>
		<option value="3">Desain Sistem Informasi</option>
		<option value="4">Desain Sistem dan Jaringan</option>
		<option value="5">Pemeliharaan & Perawatan Perangkat Keras</option>
		<option value="6">Sistem Monitor Infrastruktur TI</option>
		<option value="7">Tata Kelola TI</option>
	</select>
 </div>
 <div class="form-group">
 <label>Kategori</label><br>
 <input type="checkbox" name="category" value="1"> Resiko Finansial
 <input type="checkbox" name="category" value="2"> Resiko Operasional
 <input type="checkbox" name="category" value="3"> Resiko Reputasi
 </div>
<div class="form-group">
	<label for="kode" class="control-label">Kode Resiko</label>
	<input type="text" class="form-control" name="kode"
	placeholder="Kode Resiko">
 </div>
<div class="form-group">
	<label for="event" class="control-label">Risk Event/Uraian Peristiwa Resiko</label>
	<input type="text" class="form-control" name="event"
	placeholder="Uraian Peristiwa Resiko">
 </div>
 <div class="form-group">
	<table class="table" id="dynamic">
	<tr>
		<th>Risk Course/Penyebab Risiko</th>
		<th>Sumber Risiko</th>
	</tr>
	<tr>
		<td><input type="text" class="form-control" name="tambah[0][penyebab]" placeholder="penyebab"></td>
		<td><input type="text" class="form-control" name="tambah[0][sumber]" placeholder="Internal/Eksternal"></td>	
		<td><button type="button" name="add" id="add" class="btn btn-success">Tambah</button></td>
	</tr>
	</table>
 </div>
<div class="form-group">
	<label for="owner">Risk Owner</label>

	<select class="form-control" name="kode">
		<option value="">--pilihan--</option>
		<option value="1">SM TI</option>
		<option value="2">Manajer Aplikasi Bisnis</option>
		<option value="3">Manajer Jaringan Komputer dan Perangkat Keras</option>
		<option value="4">Manajer Perangkat Keras</option>
	</select>
 </div>

 <div class="form-group">
	<label>Nama Dept./Unit Terkait</label><br>
	<input type="radio" name="unit" value="1"> IT
	<input type="radio" name="unit" value="2"> Semua Statker
 </div>
<div class="form-group">
	<table class="table">
	<label>Inherent Risk</label>
	<tr>
		<th>Likelohood</th>
		<th>Impact</th>
	</tr>
	<tr>
		<td><input type="text" class="form-control" name="likelihoodir" placeholder="Nilai"></td>
		<td><input type="text" class="form-control" name="Impactir" placeholder="Nilai"></td>
	</tr>
	</table>
</div>
 <div class="form-group">
	<table class="table">
	<label>Existing Control/Pengendalian yang ada</label>
	<tr>
		<th>Ada/Tidak Ada</th>
		<th>Memadai/Belum Memadai</th>
		<th>Dijalankan 100%/belum dijalankan 100%</th>
	</tr>
	<tr>
		<td><input type="text" class="form-control" name="ada"></td>
		<td><input type="text" class="form-control" name="memadai"></td>
		<td><input type="text" class="form-control" name="dijalankan"></td>
	</tr>
	</table>
 </div>
<div class="form-group">
	<table class="table">
	<label>Residual Risk</label>
	<tr>
		<th>Likelohood</th>
		<th>Impact</th>
	</tr>
	<tr>
		<td><input type="text" class="form-control" name="likelihoodrr" placeholder="Nilai"></td>
		<td><input type="text" class="form-control" name="Impactrr" placeholder="Nilai"></td>
	</tr>
	</table>
 </div>
 <div class="form-group">
	<label>Risk Treatment</label><br>
	<label for="perlakuan" class="control-label">Opsi Perlakuan Resiko</label>
	<input type="text" class="form-control" name="perlakuan">
 </div>
 <div class="form-group">
	<label for="tindakan" class="control-label">Deskripsi Tindakan Mitigasi</label>
	<input type="text" class="form-control" name="tindakan">
 </div>
 <div class="form-group">
	<table class="table">
	<label>Risk After Mitigation</label>
	<tr>
		<th>Likelohood</th>
		<th>Impact</th>
	</tr>
	<tr>
		<td><input type="text" class="form-control" name="likelihoodram" placeholder="Nilai Target"></td>
		<td><input type="text" class="form-control" name="Impactram" placeholder="Nilai Target"></td>
	</tr>
	</table>
 </div>
 <div class="form-group">
 <button type="submit" class="btn btn-info">Simpan</button>
 <button type="exit" class="btn btn-danger">Batal</button>
 </div>
</form>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
	<script type="text/javascript">
    var i = 0;
 
    $("#add").click(function(){
   
        ++i;
   
        $("#dynamic").append('<tr><td><input type="text" name="tambah['+i+'][penyebab]" placeholder="Penyebab" class="form-control" /></td><td><input type="text" name="tambah['+i+'][sumber]" placeholder="Internal/Eksternal" class="form-control" /></td><td><button type="button" class="btn btn-danger remove-tr">Remove</button></td></tr>');
    });
   
    $(document).on('click', '.remove-tr', function(){  
         $(this).parents('tr').remove();
    });    
</script>
@stop