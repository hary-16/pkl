<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class registers extends Model
{
    protected $table ='registers'; //utk menghubungkan ke tabel
	protected $primaryKey='id';
	protected $fillable = ['id','tujuan','proses_id','kategori_id','kode',
			'event',
			'penyebab',
			'sumber',
			'potensi',
			'owners_id',
			'unit_id',
			'likelihoodir',
			'impactir',
			'levelir',
			'ada',
			'memadai',
			'dijalankan',
			'likelihoodrr',
			'impactrr',
			'levelrr',
			'perlakuan',
			'tindakan',
			'likelihoodram',
			'impactram',
			'levelram'];
	public function getProses()
	{
		return $this->belongsTo('App\models\proses','proses_id');
	}
	
	public function getKategori()
	{
		return $this->belongsTo('App\models\kategori','kategori_id');
	}
	
	public function getOwners()
	{
		return $this->belongsTo('App\models\owners','owners_id');
	}
	
	public function getUnit()
	{
		return $this->belongsTo('App\models\unit','unit_id');
	}
}

