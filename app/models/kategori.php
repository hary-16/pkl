<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class kategori extends Model
{
    protected $table ='kategori'; //utk menghubungkan ke tabel
	protected $primaryKey='id';
	protected $fillable = ['id','kategori'];
}
