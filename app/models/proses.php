<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class proses extends Model
{
    protected $table ='proses'; //utk menghubungkan ke tabel
	protected $primaryKey='id';
	protected $fillable = ['id','proses'];

}
