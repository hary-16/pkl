<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class owners extends Model
{
    protected $table ='owners'; //utk menghubungkan ke tabel
	protected $primaryKey='id';
	protected $fillable = ['id','owners'];

}
