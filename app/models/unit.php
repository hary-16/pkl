<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class unit extends Model
{
    protected $table ='unit'; //utk menghubungkan ke tabel
	protected $primaryKey='id';
	protected $fillable = ['id','unit'];
}
