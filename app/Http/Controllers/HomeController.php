<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\proses;
use App\models\registers;
use App\models\owners;
use App\models\kategori;
use App\models\unit;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
		$proses=proses::all();
		$owners=owners::all();
		$unit=unit::all();
		$kategori=kategori::all();
		
        return view('home',compact('proses','owners','unit','kategori'));
    }
	
	public function tampilan()
	{
		$reg=registers::all();
		return view('riskregister',compact('reg'));
	}
	
	public function store(request $request)
	{
		$this->validate($request,[
			'tujuan'=>'required',
			'proses'=>'required',
			'kategori'=>'required',
			'kode'=>'required',
			'event'=>'required',
			'penyebab'=>'required',
			'sumber'=>'required',
			'potensi'=>'required',
			'owners'=>'required',
			'unit'=>'required',
			'likelihoodir'=>'required',
			'impactir'=>'required',
			'levelir'=>'required',
			'ada'=>'required',
			'memadai'=>'required',
			'dijalankan'=>'required',
			'likelihoodrr'=>'required',
			'impactrr'=>'required',
			'levelrr'=>'required',
			'perlakuan'=>'required',
			'tindakan'=>'required',
			'likelihoodram'=>'required',
			'impactram'=>'required',
			'levelram'	=>'required',
		]);
			
		DB::table('registers')->insert([
		'tujuan'		=$request->tujuan;
		'proses'		=$request->proses;
		'kategori'		=$request->kategori;
		'kode'			=$request->kode;
		'event'			=$request->event;
		'penyebab'		=$request->penyebab;
		'sumber'		=$request->sumber;
		'potensi'		=$request->potensi;
		'owners'		=$request->owners;
		'unit'			=$request->unit;
		'likelihoodir'	=$request->likelihoodir;
		'impactir'		=$request->impactir;
		'levelir'		=$request->levelir;
		'ada'			=$request->ada;
		'memadai'		=$request->memadai;
		'dijalankan'	=$request->dijalankan;
		'likelihoodrr'	=$request->likelihoodrr;
		'impactrr'		=$request->impactrr;
		'levelrr'		=$request->levelrr;
		'perlakuan'		=$request->perlakuan;
		'tindakan'		=$request->tindakan;
		'likelihoodram'	=$request->likelihoodram;
		'impactram'		=$request->impactram;
		'levelram'		=$request->levelram;
		
		return \Redirect::action('HomeController@tampilan')
				->with('message',
							'Risk Register baru berhasil ditambahkan');
	}
}
