

<?php $__env->startSection('title', 'Dashboard'); ?>

<?php $__env->startSection('content_header'); ?>
    <h4>Daftar Risk Register</h4>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<a href="/home" class="btn btn-info btn-sm">Tambah Risk Regster</a>
    <table class="table table-responsive martop-sm">
	<thead>
		 <th>No</th>
		 <th>Objective/Tujuan</th>
		 <th>Proses Bisnis</th>
		 <th>Risk Category</th>
		 <th>Kode Risiko</th>
		 <th>Risk Event</th>
		 <th>Risk Couse</th>
		 <th>Sumber Risiko</th>
		 <th>Severity/Akibat</th>
		 <th>Qualitative</th>
		 <th>Pemilik Resiko</th>
		 <th>Unit Terkait</th>
		 <th>Score/Nilai Inherent Risk</th>
		 <th>Likelihood</th>
		 <th>Impact</th>
		 <th>Tingkat Risiko</th>
		 <th>Existring Control</th>
		 <th>Ada/Tidak Ada</th>
		 <th>Memadai/Belum Memadai</th>
		 <th>Dijalankan 100%/Belum dijalankan 100%</th>
		 <th>Score/Nilai Residual Risk</th>
		 <th>Likelihood</th>
		 <th>Impact</th>
		 <th>Tingkat Resiko</th>
		 <th>Opsi Perlakuan Risiko</th>
		 <th>Deskripsi Tindakan Mitigasi</th>
		 <th>Nilai Target</th>
		 <th>Likelihood</th>
		 <th>Impact</th>
		 <th>Tingkat Risiko</th>
    </thead>
    <tbody>

	</tbody>
	</table>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
    <link rel="stylesheet" href="/css/admin_custom.css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script> console.log('Hi!'); </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('adminlte::page', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\twc\resources\views/monitoring.blade.php ENDPATH**/ ?>