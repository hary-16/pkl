<?php $__env->startSection('title', 'Dashboard'); ?>

<?php $__env->startSection('content_header'); ?>
    <h1>Risk Register</h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="card-body">
                    <?php if(session('status')): ?>
                        <div class="alert alert-success" role="alert">
                            <?php echo e(session('status')); ?>

                        </div>
                    <?php endif; ?>
	</div>
	
<form action="/store" method="post">
<?php echo e(csrf_field()); ?>

	
	<?php if(count($errors) > 0): ?>
		<div class="alert alert-danger">
			Terdapat beberapa kasalahan pada saat mengisi yang harus diperbiki.<br><br>
			<ul>
				<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<li><?php echo e($error); ?></li>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</ul>
		</div>
	<?php endif; ?>
<div class="form-group">
	<label for="tujuan" class="control-label">Objective/Tujuan</label>
	<input type="text" class="form-control" name="tujuan" placeholder="Objective/Tujuan">
 </div>
 
 <div class="form-group">
	<label for="proses_id">Proses Bisnis</label>
	<select class="form-control" name="proses">
		<option value="">--pilihan--</option>
			<?php $__currentLoopData = $proses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<option value="<?php echo e($p->id); ?>"><?php echo e($p->proses); ?></option>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	</select>
 </div>
 
 <div class="form-group">
 <label>Kategori</label><br>
	<input type="checkbox" name="kategori[]" value="1"> Resiko Finansial
	<input type="checkbox" name="kategori[]" value="2"> Resiko Operasional
	<input type="checkbox" name="kategori[]" value="3"> Resiko Reputasi
 </div>
 
<div class="form-group">
	<label for="kode" class="control-label">Kode Resiko</label>
	<input type="text" class="form-control" name="kode"
	placeholder="Kode Resiko">
 </div>
 
<div class="form-group">
	<label for="event" class="control-label">Risk Event/Uraian Peristiwa Resiko</label>
	<input type="text" class="form-control" name="event"
	placeholder="Uraian Peristiwa Resiko">
 </div>
 
 <div class="form-group">
	<table class="table" id="dynamic">
	<tr>
		<th>Risk Course/Penyebab Risiko</th>
		<th>Sumber Risiko</th>
		<th>Potensi Kerugian</th>
	</tr>
	<tr>
		<td><input type="text" class="form-control" name="penyebab[]" placeholder="penyebab"></td>
		<td><input type="text" class="form-control" name="sumber[]" placeholder="Internal/Eksternal"></td>	
		<td><input type="text" class="form-control" name="potensi[]" placeholder="Qualitative"></td>	
		<td><button type="button" name="add" id="add" class="btn btn-success">Tambah</button></td>
	</tr>
	</table>
 </div>
 
 <div class="form-group">
	<label for="owners_id">Risk Owner</label>
	<select class="form-control" name="owners">
		<option value="">--pilihan--</option>
			<?php $__currentLoopData = $owners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $own): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<option value="<?php echo e($own ->id); ?>"><?php echo e($own->owners); ?></option>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	</select>
 </div>

 <div class="form-group">
	<label>Nama Dept./Unit Terkait</label><br>
	<input type="radio" name="unit" value="1"> IT
	<input type="radio" name="unit" value="2"> Semua Statker
 </div>
 
<div class="form-group">
	<table class="table">
	<label>Inherent Risk</label>
	<tr>
		<th>Likelohood</th>
		<th>Impact</th>
		<th>Level</th>
	</tr>
	<tr>
		<td><input type="text" id="text1" class="form-control" name="likelihoodir" placeholder="Nilai" onkeyup="kali();"></td>
		<td><input type="text" id="text2" class="form-control" name="impactir" placeholder="Nilai" onkeyup="kali();"></td>
		<td><input type="text" id="text3" class="form-control" name="levelir" ></td>
	</tr>
	</table>
</div>

 <div class="form-group">
	<table class="table">
	<label>Existing Control/Pengendalian yang ada</label>
	<tr>
		<th>Ada/Tidak Ada</th>
		<th>Memadai/Belum Memadai</th>
		<th>Dijalankan 100%/belum dijalankan 100%</th>
	</tr>
	<tr>
		<td><input type="text" class="form-control" name="ada"></td>
		<td><input type="text" class="form-control" name="memadai"></td>
		<td><input type="text" class="form-control" name="dijalankan"></td>
	</tr>
	</table>
 </div>
 
 <div class="form-group">
	<table class="table">
	<label>Residual Risk</label>
	<tr>
		<th>Likelohood</th>
		<th>Impact</th>
		<th>Level</th>
	</tr>
	<tr>
		<td><input type="text" id="txt1" class="form-control" name="likelihoodrr" placeholder="Nilai" onkeyup="kali2();" ></td>
		<td><input type="text" id="txt2" class="form-control" name="impactrr" placeholder="Nilai" onkeyup="kali2();" ></td>
		<td><input type="text" id="txt3" class="form-control" name="levelrr" ></td>
	</tr>
	</table>
 </div>
 
 <div class="form-group">
	<label>Risk Treatment</label><br>
	<label for="perlakuan" class="control-label">Opsi Perlakuan Resiko</label>
	<input type="text" class="form-control" name="perlakuan">
 </div>
 
 <div class="form-group">
	<label for="tindakan" class="control-label">Deskripsi Tindakan Mitigasi</label>
	<input type="text" class="form-control" name="tindakan">
 </div>
 
 <div class="form-group">
	<table class="table">
	<label>Risk After Mitigation</label>
	<tr>
		<th>Likelohood</th>
		<th>Impact</th>
		<th>Level</th>
	</tr>
	<tr>
		<td><input type="text" id="t1" class="form-control" name="likelihoodram" placeholder="Nilai Target" onkeyup="kali3();"></td>
		<td><input type="text" id="t2" class="form-control" name="impactram" placeholder="Nilai Target" onkeyup="kali3();"></td>
		<td><input type="text" id="t3" class="form-control" name="levelram" ></td>
	</tr>
	</table>
 </div>
 
 <div class="form-group">
	<button type="submit" class="btn btn-info">Simpan</button>
	<button type="reset" class="btn btn-danger">Batal</button>
 </div>
 
</form>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
    <link rel="stylesheet" href="/css/admin_custom.css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script> console.log('Hi!'); </script>
	<script type="text/javascript">
    var i = 0;
 
    $("#add").click(function(){
   
        ++i;
   
        $("#dynamic").append('<tr><td><input type="text" name="penyebab[]" placeholder="Penyebab" class="form-control" /></td><td><input type="text" name="sumber[]" placeholder="Internal/Eksternal" class="form-control" /></td><td><input type="text" name="potensi[]" placeholder="Qualitative" class="form-control" /></td><td><button type="button" class="btn btn-danger remove-tr">Remove</button></td></tr>');
    });
   
    $(document).on('click', '.remove-tr', function(){  
         $(this).parents('tr').remove();
    });    
	</script>
	
	<script>
	function kali() {
      var txtFirstNumberValue = parseInt(document.getElementById('text1').value);
      var txtSecondNumberValue = parseInt(document.getElementById('text2').value);
      var result = (txtFirstNumberValue) * (txtSecondNumberValue);
      if (!isNaN(result)) {
		 if(result>5)
			result = 'High';
		else if(result>3)
			result = 'Medium';
		else
			result = 'Low'
		
		document.getElementById("text3").value = result;
      }
	}
	</script>
	
	<script>
	function kali2() {
      var txtFirstNumberValue = document.getElementById('txt1').value;
      var txtSecondNumberValue = document.getElementById('txt2').value;
      var result = parseInt(txtFirstNumberValue) * parseInt(txtSecondNumberValue);
      if (!isNaN(result)) {
		 if(result>5)
			result = 'High';
		else if(result>3)
			result = 'Medium';
		else
			result = 'Low'
		
         document.getElementById('txt3').value = result;
      }
	}
	</script>
	
	<script>
	function kali3() {
      var txtFirstNumberValue = document.getElementById('t1').value;
      var txtSecondNumberValue = document.getElementById('t2').value;
      var result = parseInt(txtFirstNumberValue) * parseInt(txtSecondNumberValue);
      if (!isNaN(result)) {
		  if(result>5)
			result = 'High';
		else if(result>3)
			result = 'Medium';
		else
			result = 'Low'
		
         document.getElementById('t3').value = result;
      }
	}
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('adminlte::page', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\twc\resources\views/home.blade.php ENDPATH**/ ?>